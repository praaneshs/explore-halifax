# Explore Halifax

![Halifax waterfront](images/halifax.png)

This repository puts together a collection of recommendations to explore Halifax, NS, Canada by computer science students at Dalhousie.

## Historical places

**Citadel Hill**\
![Citadel Hill](images/citadel.jpg) <br/>
Note: This image has been taken from https://www.tripadvisor.com/Attractions-g154976-Activities-c47-Halifax_Halifax_Regional_Municipality_Nova_Scotia.html, on May 5, 2022

There are a lot of Historical Places to visit in Halifax. The Citadel Hill tops the list. It is a hill which has a harbour underneath itself.
More information about it could be found at https://www.novascotia.com/see-do/attractions/halifax-citadel-national-historic-site/1440

Citadel Hill - Historical military base in Halifax. Link: https://www.pc.gc.ca/en/lhn-nhs/ns/halifax

[**Point Pleasant park**](https://www.novascotia.com/see-do/attractions/point-pleasant-park/1461) has a few military historical sites, and a nice view of the Atlantic ocean.

[**Citadel Hill**](https://www.pc.gc.ca/en/lhn-nhs/ns/halifax) large historical hill with a great view of Halifax.

[**Halifax Public Gardens**](https://www.google.com/travel/things-to-do?g2lb=2502548,2503771,2503781,4258168,4270442,4284970,4291517,4306835,4429192,4515404,4597339,4649665,4722900,4723331,4733969,4738606,4747696,4757164,4758493,4762561,4762570,4771572,4771758&hl=en-CA&gl=ca&ssta=1&dest_mid=/m/02qjb7z&dest_state_type=main&dest_src=ts&q=halifax+historical+places&sa=X&ved=2ahUKEwiX14OklMv3AhVCkIkEHQraAFoQuL0BegQIChAv) Friends of the Public Gardens offer free horticultural and historical tours of the gardens

[**York Redoubt**](https://www.pc.gc.ca/en/lhn-nhs/ns/york/index?utm_source=gmb&utm_medium=yorkredoubt) Old military defense base.

[**Canadian Forces Base**](https://www.cafconnection.ca/Halifax/In-My-Community/About-CFB-Halifax.aspx ) Canadian Forces Base (CFB) Halifax is one of the  Canadian largest military bases and home to Canada's East Coast Navy. It was found in 1906. Now Canadian Forces Base supports Maritime Forces Atlantic 
and allocated lodger units with administrative, logistics, information technology, port operations and emergency services.

**Citadel Hill**\
At Citadel Hill visitors get the opportunity to explore the history of the fortress and the soldiers who were stationed there as well as touch a piece of Halifax's Military history\
link: https://www.halifaxcitadel.ca/

[**Halifax Seaport Farmers Market**](https://www.halifaxfarmersmarket.com/) Located on the Halifax waterfront, it has various local vendors selling a variety of goods

### Fairview Lawn Cemetery
[**Fairview Lawn Cemetery**](https://en.wikipedia.org/wiki/Fairview_Lawn_Cemetery) located at [**3720 Windsor St**](https://goo.gl/maps/EgGptiasJj1m8hgc6) Is a cemetery best known for being the final resting place of 121 victims from the sinking of the Titanic. The cemetery also contains 29 war graves of Commonwealth service personnel.
![Fairview Cemetery](images/fairviewCemetery.jpg) </br>
<small>Note: This image has been taken from https://en.wikipedia.org/wiki/Fairview_Lawn_Cemetery on May 12, 2022 </small>

### Africville Park

[**Africville Park**](https://www.pc.gc.ca/apps/dfhd/page_nhs_eng.aspx?id=1763) used to be the location of an African Nova Scotian neighbourhood before it was demolished for the purpose of urban renewal. It now hosts a community park for all to enjoy, including a mountain biking circuit.



**Point Pleasant Park**
It is a really nice park at the South End (near the port). At this park, people can walk，ride and feel the sea breeze and nature, and there are several small fortresses hidden in the woods, this is a great place to relax in your free time.

## Art gallery

### Art gallery of Nova Scotia

[**Art gallery of Nova Scotia**](https://www.artgalleryofnovascotia.ca/) With locations in downtown Halifax and Yarmouth, the Art Gallery of Nova Scotia is the largest art museum in Atlantic Canada.(from Art gallery websites)

### Studio 21
[**Studio 21**](https://studio21.ca/) is an art studio located at [**5431 Doyle St #102**](https://g.page/Studio21FineArt?share) featuring Canadian artists, with half of the artists being from the Atlantic region. The exhibitions change monthly and have shipping available worldwide.
![Studio 21](images/studio21.jpg) </br>
<small>Note: This image was taken from https://www.novascotia.com/see-do/fine-arts/studio-21-fine-art/2623 on May 12, 2022</small>


## Museums

**Thomas Mcculloch Museum**\
![Museum](images/whale.jpg) <br/>
Note: This image has been taken from https://mapsus.net/CA/thomas-mcculloch-museum-188625 on May 5, 2022
<br/>Though Halifax has a lot of Museums, however, one of the most accessible museum is within the Dalhousie University. It is
called Thomas Mcculloch Museum. This museum resides in the Department of Biology, in the Life Science Building at Dalhousie.
This museum has one of the biggest collections of birds, beetles, butterflies, moths, fishes, mushrooms species. One of the
most interesting facts about the museum is that it houses the vertebrate piece of Indian whale and the museum itself followed by Aquatron.
More information about the museum could be found at https://www.dal.ca/faculty/science/biology/research/facilities/thomas-mcculloch-museum.html

[**The Canadian museum of immigration**](https://pier21.ca/) is located next to the seaport farmers market. The museum details Halifax's long history of being a landing port for immigrants.

[**Discovery Centre**](https://thediscoverycentre.ca/) is fun and interactive science museum in Halifax.

**Canadian Museum of Immigration at Pier 21**\
Pier 21 is a National Historic site and it is site of Atlantic Canada's only national museum\
Link: https://pier21.ca/

[**Museum of Natural History**](https://naturalhistory.novascotia.ca/) a kind of science, it can help people know the natural history

### [Martime Museum of the Atlantic](https://maritimemuseum.novascotia.ca/)

Learn about the maritime history deeply interwoven into the heart of Halifax and Nova Scotia. Explore everything from how the Mi'kmaw peoples interacted with the ocean, the days of sailing, to WW1 and WW2 era Canadian naval history. Has the world's most important wooden Titanic artifacts.

## Parks

### [Fort Needham Park](https://www.halifax.ca/parks-recreation/arts-culture-heritage/halifax-explosion/fort-needham-memorial-park-master-plan)
A newly renovated park in the historic Hydrostone neighborhood of North End Halifax. This park is dedicated to the lives lost in the Halifax Explosion of 1917, and boasts a state-of-the-art playground for children of all ages.

### [Halifax Common](https://www.google.com/maps/place/Halifax+Common/@44.6499143,-63.5893351,17.58z/data=!4m5!3m4!1s0x4b5a222ad81d169d:0x381aec3ed7597adf!8m2!3d44.6499674!4d-63.5889051)

A beautiful urban park located in the center of Halifax. If you're visiting in the winter check out the outdoor skating rink at Emera Oval.

### [Point Pleasant Park](https://www.novascotia.com/see-do/attractions/point-pleasant-park/1461)

The park situated in the south end of the Halifax. If you visit there in winter, you can just enjoy a snow world in park. Walking and cycling are both accept in the park.

### [Long Lake Park](https://www.halifaxtrails.ca/long-lake-provincial-park/) 

Really quiet place with beautiful scenery. It's great place to do caoeing. It's located on westren side of halifax on dunbrack street.You must visit.


### [Horseshoe Island Park](https://www.yelp.ca/biz/horseshoe-island-park-halifax)
It is on 6939 Upper Grove, Halifax, NS B3H 2M6. It is a small park and has some small benches. You can enjoy the beautiful view of sunrise and sunset here. 

### [Public Gardens](https://www.halifaxpublicgardens.ca/)
This beautiful park is home to a wide variety of plantlife, statues many ducks, and even a tiny scale model of the Titanic floating in a pond. It also has decent Wi-Fi!

### [DeWolf Park](https://www.halifax.ca/parks-recreation/parks-trails-gardens/trails/dewolf-park-boardwalk)
The park is a peaceful place to relax and just view the activities on the Bedford Basin. At times the playground is a bit noisy but it is always a delight tom hear children enjoying themselves. 


 [**Hail Pond Park**](https://www.google.com/search?q=hail%20pond%20park&rlz=1C5CHFA_enCA991CA991&oq=hail+pond+&aqs=chrome.2.69i57j46i512j0i512j0i22i30j0i390l3.10529j0j4&sourceid=chrome&ie=UTF-8&tbs=lf:1,lf_ui:1&tbm=lcl&sxsrf=ALiCzsb1cls3VgOmvYuo2Q9bdX01oGJRKw:1652294742079&rflfq=1&num=10&rldimm=12111580312426070992&lqi=Cg5oYWlsIHBvbmQgcGFya0i8zPHV5YCAgAhaGhACGAAYASIOaGFpbCBwb25kIHBhcmsqAggDkgEEbGFrZaoBDBABKggiBHBhcmsoAA&phdesc=D4kw4nWWsW0&ved=2ahUKEwiI06nJjdj3AhWIlYkEHbU0CEUQvS56BAgPEAE&sa=X&rlst=f#rlfi=hd:;si:12111580312426070992,l,Cg5oYWlsIHBvbmQgcGFya0i8zPHV5YCAgAhaGhACGAAYASIOaGFpbCBwb25kIHBhcmsqAggDkgEEbGFrZaoBDBABKggiBHBhcmsoAA,y,D4kw4nWWsW0;mv:[[44.6335323,-63.632811700000005],[44.6332671,-63.636226199999996]]) Hail Pond park is 3.6 km a way from Dalhousie University a beautiful place for morning walk or to take your dog a walk very quiet place to be a way from traffic.

### [Shubie Park](https://www.google.com/maps/place/Shubie+Park/@44.701534,-63.5576463,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a26a25bbaf08d:0xd48b34c791afe35!8m2!3d44.7015302!4d-63.5554576)
If you are visiting Dartmouth Crossing, and you want some fresh air, Shubie Park might be the place for you. Not only you can get some fresh air, you get walk along the Shubenacadie Canal, and it cannot get better than this. Also, expect to see some wild animals as well! 
### [Belchers Marsh Park](https://www.halifaxtrails.ca/belchers-marsh/)
It is on Parkland Dr. A beautiful place to take a morning walk or walk dogs. A quite place to do meditations and relax. It gives a feeling of getting out of the city.

### [Westmount School Playground and Splash Pad](https://www.chatterblock.com/resources/55007/westmount-school-splash-pad-halifax-ns/)
Westmount is right across the street from the Halifax Shopping Centre, so easy to access by public transport, or you could combine a trip to the mall with some time on the playground. Address: 6700 Edward Arab Ave, Halifax

## Beaches

Rainbow Haven Beach - Decent beach nearby Halifax. Link: https://www.novascotia.com/see-do/outdoor-activities/rainbow-haven-beach-provincial-park/2203

[**Dingle Beach - Sir Sanford Fleming Park**](https://www.novascotia.com/see-do/attractions/sir-sandford-fleming-park-the-dingle/1521) A 95-acre park with four natural habitats (woodlands, heath barren, salt water and pond; please do not feed the waterfowl), walking trails, the Dingle Tower with bronze lions at the foot, a sandy beach (unsupervised swimming), a wharf and a boat launch.

[**Carters Beach**](https://www.sandylanevacations.com/activities.details.php?activity=carters-beach-nova-scotia) Carters Beach is a beautiful beach located in Port Mouton, Nova Scotia only a 2-hour drive from Halifax to the location worth the drive place is trash free water is transparent and crystal blue feels like you are on an island does not feel like Nova Scotia at all. 

**Clam Harbour Beach Provincial Park**\
Clam Harbour features a long white sand beach with a unique shallow tide stream that often means warmer water for swimmers.\
link: https://www.novascotia.com/see-do/outdoor-activities/clam-harbour-beach-provincial-park/1717

Lawrencetown Beach Provincial Park, it offers a really long coastline. The weather there could be very windy, check before you go.

### Black Rock Beach

[**Black Rock Beach**](https://www.halifax.ca/parks-recreation/programs-activities/swimming/supervised-beaches-outdoor-pools-splash-pads) which is at Point Pleasant Park on Sailors Memorial Way. Here is a tourist attraction in Halifax. We can walk around and feel the sea breeze.
[**Google Map Link**](https://goo.gl/maps/LEhaQpPJtXLaZBmM8)


### Chocolate Lake Beach

[**Chocolate Lake Beach**](https://www.google.com/maps/place/Chocolate+Lake/@44.6384459,-63.6259791,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a21eccc552311:0x8716c002b46e4423!8m2!3d44.6388728!4d-63.6236068) is an ideal beach resort to drive to with friends or family for a swim on a hot summer day, and it's only a 5-minute drive from the city center.

[**Conrad’s Beach**](https://scotiasites.com/conrads-beach/) a really good place to watch the sunset with your friends. Or to just take a nice walk, the water is a little brisk and usually only gets warm around August.

[**Crystal Crescent Beach**](https://parks.novascotia.ca/park/crystal-crescent-beach) Three white-sand beaches near the mouth of Halifax Harbor. Great place to go on a sunny day.

[**Queensland Beach**](https://www.google.com/maps/place/Queensland+Beach+Nova+Scotia/@44.6351658,-64.0438072,14z/data=!3m1!4b1!4m5!3m4!1s0x4b59ec17e9ca0f8b:0xa54749506702ad77!8m2!3d44.6351677!4d-64.0262976) is a bit further from the heart of the city, but well worth it. Make a trip to Queensland on a hot summer day with family and friends. This beach is supervised, but becomes packed with people very quickly!

###Williams Lake Beach
[**Williams Lake Beach**](https://www.gov.mb.ca/sd/parks/park-maps-and-locations/western/william.html) William Lake is east of the main Turtle Mountain Provincial Park. It is well-known for its stocked waters which have produced great catches of rainbow trout over the years.
## Restaurants

[**Swiss Chalet**](https://www.swisschalet.com/)is a Canadian chain of casual dining restaurants. The roast pork ribs are delicious.

[**Afrite**](https://afrite.ca/) is a restaurant located at [**1360 Lower Water St**](https://g.page/Afriteresto?share) owned by Masterchef Canada contestant (Seasons 2 and 7) Andrew Al-Khouri. Afrite showcases modern Mediterranean fusion based on Andrew’s mother’s traditional Syrian cooking style. <br/>
![Afrite](images/afrite.jpg) <br/>
<small>Note: This image has been taken from https://afrite.ca/about/ on May 12, 2022 </small>

[**Café Aroma Latino**](https://www.cafearomalatino.com/) is a great spot to try latin american cuisine, there is a variety of dishes from breakfast to lunch that can be enjoyed with region specific drinks. In addition to serving food you are able to purchase some grocery items from these countries.

[**Boondocks**](https://www.boondocksrestaurant.ca/)
Classic Seafood Restaurant on the shore of the inlet, facing McNabs island. The seafood chowder,
and the fish pie are my favourite. Just ask too add salt (They dont put enough).

**Battery Park Bar and Eatery**\
![Battery Park Bar](images/bar.jpg) <br/>
Note: This image has been taken from their website https://batterypark.ca/ on May 5, 2022

There is a lot of interesting food that could be found in Halifax. Multiple cuisines including Indian, Korean, Japnese, French could be found.
The best place to find cheap and interesting Nachos is Battery Park Beer Bar and Eatery in Dartmouth. The menu and the further details of the bar could be accessed from their website: https://batterypark.ca/
They serve till 11:00 PM, however it is open till 12:00AM. It is the first come and first serve basis and hence no reservations are required.

[**Good Robot**](https://goodrobotbrewing.ca/)
![picture of Good Robot patio, with customers drinking in the sun](images/good-robot-image.jpeg)
<small>image taken from: https://bikefriendlyns.ca/node/111 on May 10th</small>\
Good Robot is a restaurant and bar in the North End of Halifax, started by 3 engineers who were bored of the monotony of their jobs. They came together to create a funky robot themed establishment with a kickin outdoor patio lovingly called the GastroTurf as it is fully astroturf grass.

[**Darrell’s**](https://darrellsrestaurants.com/) restaurant is a great diner type place which serves award-winning burgers and shakes. My go to when going to Darrell's is their award-wining peanut butter burger.

[**Feasts**](https://www.thefeasts.ca/) All dishes are made with delicious fresh ingredients right in front of your eyes! Can be customized as per your preferences:
s
Mappatura - Italian restaurant on Spring Garden Road. Link: https://mappaturabistro.ca/

### Charger Burger 

[**Charger Burger**](https://www.chargerburger.com/) is one of the few Halal burger restaurants in Halifax. They use fresh ingredients from top quality Nova Scotia producers. They have a great dining area with friendly staff.

[**Song's restaurant**](http://www.songskoreanrestaurant.com/) is a small Korean restaurant with a nice atmosphere and good food.

[**JUKAI Japanese & Thai**](https://jukaidartmouth.com/), all you can eat for 29.99 for each person, best sushi i had in HRM

[**Casablanca**](https://casablancahfx.ca/) is an amazing restaurant with great interior that offers delicious and authentic Moroccon food in Halifax.

[**5 Fisherman restuarant**](https://www.fivefishermen.com/) is a place only provide seafood.

[**The Foggy Goggle**](https://www.thefoggygoggle.ca/) The Foggy Goggle serves up a delicious assortment of healthy pub fare classics with a twist. Vegan and gluten-free options available.

[**9+nine**](https://www.9plus9.ca/) is a place to have authentic Chinese food.

[**Bakeaway**](https://www.bakeaway.ca/) Bakeaway is a Middle-Eastern cuisine that contains alot of the most famous foods that are famous
throughout the arab world. It makes in my opinion and other opinions, the best Middle-Eastern dishes in Halifax. It contains dishes such as
Falafel wraps, Shawarma, Vine leaves and many more that are worth the money for.

[**The Keg**](https://kegsteakhouse.com/en/locations/halifax) is a good steak house. They have delicious food, and it is a really prefect place to date. I bet this is the best steak house in Halifax.

[**Cha Baa Thai Restaurant**](https://chabaathairestaurant.ca)  
A restaurant that serves authentic Thai dishes and is recommended to people who like food with a spicier flavoring to satisfy their palates. Their Red Curry Noodle Soup and Pad Thai is especially delicious.

[**Sushi Nami Royale**](https://sushinami.ca/) Let's start off by mentioning how its one of the best sushi restaurants in the city by far, everything is fresh, great servers, and great food price isn't to high, but definitely high quality sushi amazing drinks as well.

[**Buta Ramen**](https://www.butaramen.ca) is a good place if you're craving some ramen. They've got other appetizers too like edamame

[**902 restaurant & catering**](https://www.902halifax.com/media) 902 restaurant & catering is Middle Eastern restaurant makes Middle-Eastern Lunch and dinner meals such as Kebabs, Lamb Shank, Roasted chicken, Traditional Dishes, Fried Chicken, Shawarma, Hummus, Wraps, Burgers, it is located at spring garden at 1579 Dresden Row, Halifax, Nova Scotia B3J 2K4

[**Timberlea Tavern**](https://www.facebook.com/thetbr/) is a great place place to enjoy bar foods such as wings, burgers, nachos as well as sandwiches, salads and much more while enjoying a few drinks. Be sure to check their event schedule for live performers.

[**The Board Room Cafe**](https://theboardroomgamecafe.mybigcommerce.com/) is a boardgame cafe located on Barington St. There's a wide variety of games you can play with friends for only $6 per session, plus a menu of snacks, meals, and drinks to keep you going!

[**La Frasca Cibi & Vini**](https://lafrasca.ca) A beautifully designed Italian restaraunt located in the core of Halifax's downtown area at 5650 Spring Garden Rd that combines classic Italian dishes with a modern twist.  A lovely ambience creates a perfect location for a date night or anniversary meal!

[**Boston Pizza**](https://bostonpizza.com/en/locations/downtown-halifax.html) Boston Pizza located on Granville Street has a variety of delicious pizzas and fries, as well as a wide selection of drinks. The clean environment inside the hotel makes you feel comfortable. Customers can customize the pizza they want according to their preferences, 
one of the best pizza places in the Halifax downtown.

[**Fan's Chinese Restaurant**](http://www.fansrestaurant.com/) A Chinese restaurant located on Windmill Road in Dartmouth serving dim sum, hotpot, and other dishes. The best dim sum I've had in HRM with lots of selection.

###Shadia's Pizza

[**Shadia's Pizza**](https://shadias-pizza.com) the best place to get pizza from, not only have pizza but also burgers and proteins. Highly recommended. Fast delivery and delicious food!. 

###Masala Delight
[**Masala Delight**](https://www.masaladelight.com/) Its a great place for indian cusine especially south indian. They have one of the best dosas in halifax.

### Busan Korean BBQ 
[**Busan Korean BBQ**](http://busankoreanbbq.ca/) is a great korean barbeque place to go out with family or friends. It has a lunch special menu at 12:00-4:00pm for affordable and delicious korean dishes.

###Tawa Grill
[**Tawa Grill**](https://176838.com/tawa) makes classic Indian cuisine, great portions and taste for reasonable pricing. They had a day-time lunch buffet for $14, they unfortunately don’t have it anymore. Its ambience makes for a very romantic date spot after evening, and the staff are super nice.

### Wineries

![picture of the Benjamin Bridge patio at sunrise](images/Benjamin-Bridge-at-Sunrise.jpeg) <small>Image taken from https://winesofnovascotia.ca/portfolio/benjamin-bridge/ on May 10th</small>  
[**Benjamin Bridge**](https://benjaminbridge.com/) is an award-winning vineyard located in the heart of Gaspereau Valley, known for it's sparkling whites. You might know it as the brand behind the locally loved Nova 7 wine. They have recently been the buzz of the town as they have introduced the alcohol-free, wine-style beverage Piquet Zero, now being sold at grocery stores near you.

[**L'Acadie Vineyards**](https://www.lacadievineyards.ca/) Located in the beautiful Annapolis Valley. L'Acadie was one of the first the first organic winery in Nova Scotia and has won many awards for their wines in Canada and Internationally. It is definetly worth a stop in for a tasting session if you can make it!

[**Luckett Vineyards**](https://www.luckettvineyards.com/) Located just north of Windsor, overlooking the Gaspereau Valley.  The estate first started growing grapes in 2003, but did not open their doors to the public until 2011, this is a family run vineyard that boasts a lovely restaraunt.  A must stop location for wine tasting in Nova Scotia!


[**Xiaoyu Homestyle Noodle**](https://goo.gl/maps/Mx5zb61Q39ZisTGV7) is an authentic chinese restaurant. You can get some real Chinese style noodles hear. There are some really spicy food.


[**The Wooden Monkey**](https://www.thewoodenmonkey.ca/) 

is really good restaurant esspeccially for seefood. It has two branch one in halifax and one in dartmouth. They use locally grown oraganic product.

[**Sushi Jet**](https://goo.gl/maps/VskYQxuoTvEWfHDq8) is a very good Japanese restaurant, very convenient and affordable, I especially like their "Sushi Appetizer".

[**Bap House Korean Kitchen**](https://www.baphouse.ca) is a Korean restaurant located on Quinpool Road. It serves with healthy and fresh food. You can also create your own bibimbap!

### Kor-B-Q
[**Kor-B-Q**](https://korbq.ca/)  is a Korean BBQ restaurant located at 278 Lacewood Dr. Halifax, this Korean Grill restaurant serves All-you-can-eat Korean BBQ, the food there taste very good, they have fresh ingredients and premium raw meats, and the price is not very high.
### Chkn Chop

[**Chkn Chop**](https://www.chknchop.com), as implied by the name, is a chicken-oriented restaurant in the north end of Halifax. They serve variety of sandwiches, poutine, cooked chicken, drinks and more. They also have weekly rotations on their sandwiches and wings every Tuesday.

[**HAPPY VEAL HOT POT**](https://happyvealhotpot.com/) This is a very good Chinese restaurant, I often order their food on the takeaway platform, the taste of his food is very good, the taste is also very fragrant, the price is also very good value, is a restaurant that I like very much

[**in spring fine asian fusion cuisine**](http://www.inspringhotpot.com/) If you want to try hot pot or spicy hot pot, you can try this restaurant, he offers great tasting sesame sauce, and the key is that he is buffet!

[**Shanxi Paomo**](https://shanxipaomo.com/) This is a great Chinese restaurant in Quinpool, the Roujiamo (kind of like Chinese burger) and Liangpi (something like cold rice noodles) taste amazing, and the price is not expensive!

[**Lot Six**](http://lotsix.ca/) Yummy place to eat and they have bottomless mimosa brunch on the weekend. It’s also a nice date night spot.

[**Eliot and Vine**](https://eliotandvine.com/) Located on clifton street, Eliot and Vine serves a variety of amazing dishes in a cozy yet fancy enviroment

[**Fries&Co**](https://friesnco.com/menu.html) Located in 2603 Connolly St. It mainly sells the fries and fried fish. It is one of the highest rating fried fish store in Halifax.

[**Woody’s BBQ**](https://www.yellowpages.ca/bus/Nova-Scotia/Dartmouth/Woody-s-Bar-Bq/7933688.html) Their fried corn-on-the-cob with garlic butter is a delicious and unique side. They are open nightly all week. The address is 159 Hector Gate, Dartmouth, NS B3B 0E5.

### Gangnam Korean BBQ Halifax

[**Gangnam Korean BBQ Halifax**](http://gangnamkoreanbbq.ca/) is the best Korean restaurant I think. It's in 1261 Barrington St. it has the best Korean BBQ &amp; cuisine. The ingredients are very fresh and the service is very friendly

### Jean's Chinese restuarant

[**Jean's Chinese restuarant**](https://www.jeansrestaurant.ca/) has many delicious Chinese food, you can choose to order food on the official website or go to the store to taste it at 5972 Spring Garden Rd.

### Mary's Place Cafe II

[**Mary's cafe**](https://www.facebook.com/marysplacecafe2/) is a very nice diner that is close to Dalhousie university, and thus is a perfect place for students to visit. The food at the Mary's is very tasty and well priced and contains lots of different types of food in each dish. The people working there are very friendly. location on [**map**](https://g.page/marysplacecafe2?share).

[**Armview Restaurant & Lounge**](http://www.thearmview.com/) is an old-school diner that overlooks the arm. They serve breakfast, sandwiches, steaks and burgers.

[**Antojo Tacos & Tequila**](https://antojo.ca/) serves vibrant, Mexican-inspired dishes, plus an impressive collection of tequila and mezcal — all in a stunningly designed, eclectic setting.

[**Friend's Chinese restaurants**](https://www.google.ca/maps/place/Friend's+Chinese+restaurants/@44.6378966,-63.5757484,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a2237c85cfda7:0xce20274e7dcee6e8!8m2!3d44.6379111!4d-63.573569?hl=en) is a Chinese restaurant which is populator in Chinese students. This restaurant also offers takeaway service.

[**Edna**](https://www.ednarestaurant.com/) is a bustling one-room restaurant with a comfy bar. They have a neighbourhood bistro serving locally sourced and seasonally inspired dishes.

### CUT Steakhouse

[**CUT Steakhouse**](https://www.rcr.ca/restaurants/cut-steakhouse/) is one of the most famous steak restaurants in town. Also, this restaurant has the dry aged steak, where you can enjoy the different flavour of steak in Halifax. 

### The Red Asian Fusion Restaurant

[**The Red Asian Fusion Restaurant**](https://goo.gl/maps/A6ACrbmphh6cgqmX7) This Chinese restaurant is my frequent go-to. Every dish of theirs is delicious and large. The Farmhouse's Fried Pork is my most recommended dish.

### Qiu Brothers Dumplings

[**Qiu Brothers Dumplings**](https://qiubrothersdumplings.com/) which is at 1335 Barring Street. Same as the restaurant name, there have some delicious Chinese-style Dumplings. Otherwise, there also have some other Chinese food.
[**Google Map Link**](https://goo.gl/maps/5BbUgWvko2GJfkwn9)


### Yonghe Noodle House

[**Yonghe Noodle House**](https://goo.gl/maps/wpP3QUkgGCHoF7uk6) This noodle restaurant is my favorite noodle restaurant, and I like their curry chicken noodles the most.

### Tony's EST.1976

[**Tony's**](https://www.tonysdonair.ca/) is a local donair and pizza shop in Halifax, opened in 1976. Highly recommend their TONY's FAMOUS DONAIRS. 

### Your Father's Moustache

[**Your Father's Moustache**](http://yourfathersmoustache.ca/) established in 1989, is a family owned pub. The offer live music as well as outdoor seating. The chicken wings are very good!

### Jukai

[**Jukai**](https://jukaidartmouth.com/) I recommad this Jukai all you can eat sushi resturant to you, it is a really good resturant that I always visit. The location is 49 Kings Wharf Place, Dartmouth, NS.

## Night Clubs

[**The Dome**](https://www.thedome.ca/) Known by many as "Home", the Dome is a staple nightclub of downtown Halifax. A must see for any dance enthusiast or audiophile looking for a thoroughly mediocre experience.

[**The Seahorse Tavern**](https://www.facebook.com/TheSeahorseTavern/) is a hotspot for live music and some of the best parties in the city.

[**Pacifico**](https://www.pacificohalifax.com/) Located on George st in the heart of downtown, Pacifico is famous among locals and vistors for their incredible live Jazz nights on Thursdays.

### Midtown-Boomers Tavern
[**Midtown-Boomers Tavern**](http://www.themidtown.ca) The Midtown Tavern & Lounge is located in downtown Halifax, Nova Scotia directly across from the Dome / Cheers night club. Offering late night beverages and drink specials daily. Great atmosphere to drink and dance with great company!
## Universities

[**Mount Saint Vincet University**](https://www.msvu.ca/) A Primary Undergraduate university which locate at 166 Bedford Hwy, Halifax.

[**Dalhousie University**](https://www.dal.ca/) A Medical Doctoral university which locate at 5849 University Ave, downtown Halifax. It has three different campus. It is the third oldest university in Nova Scotia and the fifth oldest university in Canada.

[**Saint Mary's University**](https://www.smu.ca/) A Primary Undergraduate university which locate at 923 Robie St near dalhousie university.

[**Acadia University**](https://www2.acadiau.ca/home.html) A public undergraduate university ranked among the top Canadian institutes is located in Wolfville, N.S.

[**Nova Scotia Community College**](https://www.nscc.ca/) Nova Scotia Community College is located at the end of Barrington St. It contains 130 programs at 14 different campuses in Nova Scotia. NSCC is one of the known Univerisities found in Halifax as it allows their students tobe flexible and offers alot.

**[NSCAD University](https://nscad.ca/)** also called the Nova Scotia College of Art and Design, is a post-secondary art school in Halifax, Nova Scotia, Canada. NSCAD offers an interdisciplinary university experience unlike any other art school in the country.

[**Université Sainte-Anne**](https://www.usainteanne.ca/campus/halifax) Université Sainte-Anne is a french-language university located in 1190 Barrington St.

## Tours

[**Harbour Hopper**](https://www.harbourhopper.com/) Located on the iconic waterfront, the 55 minute number #1 boat tour in Halifax operates between the months of May->October.

[**Tall Ship Silva**](https://www.ambassatours.com/experiences) This is a great option to get out on the Halifax harbour and see the city from a different perspective. You can also enjoy a beverage while you take in the sights and are shown the landmarks of Halifax.

## Sports & competition

[**Ski Wentworth**](https://skiwentworth.ca/) This is a great ski hill located about an hour and a half north from Halifax. It is close to the town of Truro and offers many different types of trails and riding conditions.

[**Ontree Park**](https://www.ontreepark.com/) is a place to have outdoor activities in woods.

[**Ski Martock**](https://www.martock.com/) It is my favorite place to go in winter, you can choose ski or snowboard here, the experience is amazing!

### Emera Oval

[**Emera Oval**](https://www.halifax.ca/parks-recreation/programs-activities/outdoor-recreation/emera-oval) is a public outdoor skating surface. We can do in-line skating, cycling, and skateboarding in the summer and ice skating in the winter. 

### Seven Bays Bouldering

[**Seven Bays**](https://sevenbaysbouldering.com) is an indoor bouldering rock climbing gym with an attached cafe and sitting area. Bouldering is the style of rock climbing where you don't attach youself to the wall with a harness, and only climb about 20 feet up. It has a chill vibe and the rock climbing routes range in difficulty from beginner to expert.

[**Halifax Wanderers Stadium/Grounds**](https://hfxwanderersfc.canpl.ca/stadium-profile) Located in the core of downtown Halifax, besides the Halifax Public Gardens, this small stadium is home to the Halifax Wanderers soccer team. 

### East Coast Surf School

[**East Coast Surf School**](https://ecsurfschool.com) is a great way to get into surfing. They give daily surf lessons at Lawrencetown beach and offer rentals if you like to surf but don't have the gear. Surfing is a cool and fun summer activity to try with your friends or family.

### Halifax Junior Bengal Lancers

[**Halifax Junior Bengal Lancers**](http://www.halifaxlancers.com/) is a non-profit horseback riding school with both youth and adult programs. Located in the centre of downtown Halifax, Lancers is a great place to visit to watch horseback riding right in the city.  

## Hikes

### Duncans Cove

[**Duncans Cove**](https://www.halifaxtrails.ca/duncans-cove/) is a seaside hike along the coastline just south of the Halifax harbour. It connects to two old bunkers as it winds along the rocky shoreline for about 5km

[*Heart Rock / Collpitt Lake Trails*](https://www.halifaxtrails.ca/) 
Large Wilderness Park, Volunteer Run. These trails are mainly for Mountain Bikers.


### [Chain of Lakes Trail](https://www.halifaxtrails.ca/chain-of-lakes-trail/)

Chain of Lakes Trail is an easy 7.3 km trail passing by multiple lakes in and around Halifax. The trail begins on Joseph Howe Dr, across from the Atlantic Superstore, and ends on Lakeside Park Dr at the entrance of the Beechville Lakeside Timberlea trail.

### Hobsons Lake Trail

[**Hobsons Lake Trail**](https://www.google.com/maps/place/Hobsons+Lake+Trail/@44.683627,-63.7230048,14z/data=!4m13!1m7!3m6!1s0x4b598a67f4d1e3d3:0xf644dde18090792a!2sHobsons+Lake+Trail,+Nova+Scotia+B4B+1S8!3b1!8m2!3d44.6881066!4d-63.7055326!3m4!1s0x4b598a41d92a3f9f:0x87ada53da158abac!8m2!3d44.694922!4d-63.7047474) is about a 20-minute drive from downtown Halifax and is a great outdoor area with lots of rugged trails for adventure lovers to explore.

[**Hemlock Ravine Park**](https://www.novascotia.com/see-do/trails/hemlock-ravine-park/6119) Located on bedford highway, this park contains a unique heart shaped pond along with a variety of trails surrounding it

[**Polly's Cove**](https://www.halifaxtrails.ca/pollys-cove/) a park with 3.9km of marked trails just 2km away from the world famous Peggy's Cove.

[**Long Lake**](https://www.halifaxtrails.ca/long-lake-provincial-park/) a large provincial park with many trails surrounding Long Lake, only a 15 minute drive from downtown!

[**Salt Marsh Trail**](https://www.halifaxtrails.ca/salt-marsh-trail/) A trail that was previously a railway. About 9km long and is quite flat.

### Shaw Wilderness Park
[**Shaw Wilderness Park**](https://www.natureconservancy.ca/en/where-we-work/nova-scotia/featured-projects/Shaw-wilderness-park.html) is a great hiking ground to explore the landscape of Nova Scotia. There is a short 3km trail which allows you to explore nature, and see amazing views. 

### Scotsbay

[**Scotsbay**](https://gitlab.com/daluniversity/explore-halifax.git) If you like hiking, scotsbay is a place that I recommand to you, you can hiking near the beach about 4 hours long. There will be an amazoning view at the end of the hiking trip.

## Dessert Shops

### Cora Breakfast and Lunch

[**Cora**](https://www.chezcora.com/en/breakfast-lunch-restaurants/cora-clayton-park-halifax/?utm_source=googlemaps&utm_medium=nova-scotia-halifax-lacewood&utm_campaign=website) may be disguised as a family restaurant but do not be fooled. Cora is a desert shop at its heart. Nearly all the dishes have fruit and deserts at the center with meat used as decorations.

### Dairy Bar

[**Dairy Bar**](https://www.instagram.com/dairybarhfx/?hl=en) is a seasonal ice cream shop on Spring Garden road. It has delicious ice cream, sundaes, and drinks that are worth the wait in their long lines. Dairy Bar is closed in the winter but will be open again in the summer.

### Cacao 70

[**Cacao 70**](https://cacao70.com/en/our-locations/upper-water-st) is located on the Halifax waterfront, and is an ice cream bar and dessert shop. Open year-round, Cacao 70 is a great place to visit with friends. 

## Coffee Shops

[**Wired Monk Bistro**](https://www.facebook.com/wiredmonkhalifax) is a small local coffee shop and bistro located in downtown Halifax.

[**Uncommon Grounds**](https://theuncommongroup.com/pages/uncommon-grounds) is a lovely little coffee shop which is a great place to get out of the apartment to study with a warm drink and great food.

[**Coburg Social**](https://coburgsocial.com/) is a great cafe right near Dalhousie's campus. They have a great selection of food and drinks that can be enjoyed inside or on their patio. This cute cafe also sells fun cocktails and other alcohol, and on occasion hosts live music events.

[**Cabin Coffee**](https://cabincoffeehalifax.com/) Cabin Coffee is a one of the oldest family-owned coffee shop located in downtown halifax.

[**Good Luck Cafe**](http://www.manualfoodanddrinkco.com/goodluck) Located at 145 Portland St in downtown Dartmouth, this cute cafe has everything from coffee and pastries to fresh produce and take home meals.

[**Tsujiri**](https://www.tsujiri.ca/) A cafe and desserts shop located in The Curve on South Park Street. They specialize in matcha and sell drinks, sweets, and snacks inspired by traditional Japanese flavours. 

[**The Daily Grind Cafe & Bar**](https://thedailygrindcafebar.ca/) is located near the waterfront and is a great place to study or just catch up with a friend. As the name mentions, it is not only a cafe, but a bar as well which gives you the option to have your favourite beer while enjoying a good book.

### Pane ē Circo
[**Pane e Circo**](https://www.panecirco.ca) Pane e Circo is Halifax, Nova Scotia's newest Italian Salumeria, serving up highest quality products to-go, including a vast selection of imported cured meats & cheeses, sourdough breads & artisanal pastries, and even the fresh handmade pastas & sauces you’ve come to love in our Bertossi Group Restaurants! If you get a chance to try, I would get the almond croissant or any of the sandwiches!


### Rabbit Hole Cafe

[**Rabbit Hole Cafe**](https://www.rabbitholecafe.ca/) An elegant Cafe & Dessert Bar located in the heart of downtown Halifax, 1452 Dresden Row. A great rest stop to have a quiet study session, or to have your own personalized cake designed to your liking.

[**Glitter Bean Cafe**](https://www.glitterbeancafe.com) is a coffee shop on Spring Garden Road. It often has a seasonal drink on the menu and has a lot of seating for anyone who is looking to study.

[**The Kiwi Cafe **] (https://www.thekiwicafe.com)

is in chester which is south shore of nova scotia , beatiful place to have coffee and they haveinternational themes as well. They are located right next to sea which is quite nice if you are beach or sea person.

[**Dilly Dally Coffee Cafe**](https://my-site-106684-102240.square.site) is a coffee shop located on Quinpool Road. It provides a chill atmosphere with cute decorations. You can grab a coffee or some amazing baked goods here!

[**LF Bakery**](http://lfbakeryhalifax.com/) is a really yummy bakery where they have a bunch of authentic French pastries and is on Gottingen street in the North end.

### The Bread Lounge Bakery
[**The Bread Lounge**](https://thebreadloungebakery.square.site/) is a bakery and coffee shop located in downtown Halifax on Demone Street. They have pastries, desserts, sandwiches, soup, coffee, and much more. A great place to study, meet with friends, or grab a quick lunch or snack!

## Bubble Tea Shops

[**TSUJIRI**](https://www.instagram.com/tsujiri_ns/?hl=en) is located at 1605 South Park St. It sells mainly Japanese traditional matcha tea and desserts of matcha flavour.

[**Chatime**](http://www.chatimeatlantic.ca/) is located on 1480 Brenton Street. It's a cool shop for getting bubble tea and it has a lot of variety in teas this is what makes it quite good.

[**Zenq**](https://zenqhalifax.com/) is located on 1065 Barrington St. It servers Taiwanese Bubble Tea and Dessert. 

### Hitea

[**Hitea**](https://www.google.com/maps/place/Hi+Tea/@44.6713508,-63.6765064,15z/data=!4m5!3m4!1s0x0:0x32f9fe93fde15b3!8m2!3d44.6713508!4d-63.6765064) Hitea is a really good bubble tea shop and it is located at 480 Parkland Dr, Halifax.

## Tourist spots

[**Victoria Park**](https://en.wikipedia.org/wiki/Victoria_Park,_Halifax,_Nova_Scotia) is a small park that seems like not much at first glance. However, this park is home to a lot of wildlife with a special abundance of pigeons. If you are ever bored and looking for a fun time, visit the Dollarama down the street on spring garden and pick up a bag of bird seed, and you will make many friends in this park

[**Petit Passage Whale Watching**](https://www.ppww.ca) is a place to watch real whale.

[**George's Island**](https://www.pc.gc.ca/en/lhn-nhs/ns/georges/visit) This national park is a quaint little island in the Halifax harbour. Accessible only by boat (or harbour hopper!) this island is rich in history and bio-diversity as it is reportedly [**infested**](https://www.saltwire.com/nova-scotia/news/halifaxs-historic-snake-laden-island-will-be-open-to-the-public-next-summer-340147/) with snakes!

## Lighthouse

[**Peggy's Cove Lighthouse**](https://www.novascotia.com/see-do/attractions/peggys-cove-village-and-lighthouse/1468) Peggy's Cove Lighthouse, also known as Peggy's Point Lighthouse, is one of Nova Scotia’s most well-known lighthouses and may be the most photographed in Canada. (from Nova Scotia website)

### Halifax Waterfront

[**Halifax Waterfront**](https://discoverhalifaxns.com/explore/halifax-waterfront/) is a popular attraction spot for tourists. The waterfront contains a boardwalk that stretches several kilometers. This location also houses several popular restaurants and iconic structures like 'The Wave'.

## Buildings

[**The Vuze**](https://www.templetonproperties.ca/apartments-for-rent-halifax/the-vuze) What's special about this building its not something
casual that you would see throughout Nova Scotia. From it's name "The Vuze", Vuze is also spelt as Views which technically from its name you
can tell its like saying the Views. The views is the tallest building in Halifax. Not just Halifax, it's the tallest building in ALL of Nova
Scotia. It contains 28 floors for normal residents, and 5 pent houses. In total its 33 storeys high above the ground. You can find a picture
of The Vuze taken by me while walking in the images folder.

[**The Jade**](https://thejadehalifax.ca/) This is a new building which is located on Barrington St. It is one of the most luxury apartments building in Halifax and has all new facilities. Also, it has a good view of the street or the sea. Here you can enjoy the most comfortable living environment.

[**Atlantic Hotel Halifax**](https://www.atlanticahotelhalifax.com/) This black and white hotel is built on a bustling and lively street. With comfortable rooms and moderate prices, it has become the best choice of many tourists. 
The hotel's excellent location allows people to quickly go to the famous scenic spots in the city.  At the same time, you can overlook the night view of the city at night. Customers can experience comfortable treatment at an inexpensive price.

[**The Paramount**](https://paramount.universalgroup.ca/) It's an apartment in downtowm of Halifax. It has very convenient transportation. As it's in the downtown so if you want to live here it's very convenient to shop

[**Halifax Town Clock**](https://www.pc.gc.ca/apps/dfhd/page_fhbro_eng.aspx?id=10277) is one of the most recognizable landmarks in the historic urban core of Halifax. Situated nearby Citadel Hill, tourists love to go to the Town Clock to get a better view of the landscape of downtown Halifax.

## Libraries

### Halifax Central Library

[**Halifax Central Library**](https://goo.gl/maps/qKzCZeZMX1jdcRqV8) is a great library. I have been satisfied with each of my visits to the library. the staff and volunteers are very helpful. And using the printing services is always a breeze in the HCL.

### Wallace McCain Learning Commons

[**Wallace McCain Learning Commons**](https://libraries.dal.ca/hours-locations/wmlc.html) is also one of the dalhousie libraries and it is a very quiet place to study and you can also borrow a computer and book a study room.

### KILLAM LIBRARY

[**KILLAM LIBRARY**](https://www.dal.ca/campus-maps/building-directory/studley-campus/killam-library.html) is my favorite place to study, it is in 6225 University Avenue. Killam library has big space and quiet environment. In particular, you can book study rooms.

[**Kellogg Library/Learning Commons**](https://libraries.dal.ca/hours-locations/kllc-cheb.html) Another Dalhousie Library in the CHEB on Carleton campus. It is bright and modern and has lots of study spaces and rooms that can be booked.

### Patrick Power Library
[**Patrick Power Library**](https://goo.gl/maps/s8omy9N22R199qy67) is library of Saint Mary's University. There are sofa in it. It is a peace and comfortable library.

### Keshen Goodman Public Library

[**Keshen Goodman Public Library**](https://www.halifaxpubliclibraries.ca/locations/kg/) lots of amazing books to borrow, many servsee to help newcomers and students. Has a kids section and section to study. This library is one of my personal libraries to study; libraries in downtown Halifax could get loud sometimes and Keshen Goodman Public Library should be a lot quieter. 

### Halifax North Memorial Public Library

[**Halifax North Memorial Public Library**](https://www.halifaxpubliclibraries.ca/locations/n/) it is also another library in comparision to Halifax Central Library. Closer and more accessible for people who live in the north end.

### Bedford Public Library

[**Bedford Public Library**](https://www.halifaxpubliclibraries.ca/locations/BED/) is a library located on Dartmouth located, near the Sunnyside Mall. It's easily accessible through various routes coming from Dartmouth and Bedford. It's a fairly large library with a section for quiet reading along with a section for children to be kept occupied.





## Gyms

### GoodLife Fitness

[**Goodlife Fitness**](https://www.yelp.ca/biz/goodlife-fitness-halifax-12) is one of my favorite places to go. It's in 5657 Spring Garden Rd. it has good fitness equipment, but if you need a coach, it is not cheap.

### Dalplex

[**Dalplex**](https://athletics.dal.ca/facilities/Dalplex.html), situated just across the Studley campus of Dalhousie University, is one of the more popular gyms of central Halifax. Dalplex consists of pools, courts, gym equipment and much more, but is often busy and requires a membership for access.

[**YMCA HEALTH & FITNESS**](https://www.ymca.ca/what-we-offer/health-and-fitness) there is only one location at Halifax for YMCA HEALTH & FITNESS it is mix gym for male and female open every day Wednesday-Friday from 5:45a.m.- 10 p.m. located at 5640 Sackville Street, Halifax NS B3J 1L2 it has 4.5/5 stars a lot of people recommended and they say the staffs are very friendly and nice feel welcoming.

[**House of eights**](https://houseofeights.com/class-schedule/)
a place to dance and relax, make friends with same interests 


[**Canada Games Centre**](https://canadagamescentre.ca/) is a sports and recreation centre located near Clayton Park in Halifax. The centre was built for the 2011 Canada Games which took place in Halifax. It is now used as a recreational facility, offering a pool with several aquatics programs, a running track, and a gym. One of the most impressive things about the building has to be the water slide which starts inside the building, travels outside for a few loops, before re-entering the building proper
### Fit4Less

[**Fit4Less**](https://www.fit4less.ca/) has multiple locations around the HRM. An affordable alternative to Goodlife starting at just 6.99 every two weeks.

### 30 Minute Hit

[**30 Minute Hit**](https://www.30minutehit.com/) Great gym location, especially for those that enjoy boxing you could get an amazing workout here plus great staffs.

[**Queensbury Rules Boxing**](https://www.qrulesboxing.com/) is a gym with a variety of classes and an option for sparring for boxers at every level. Trials start at $25 + tax for two weeks and student discount is also offered when purchasing a membership.

### JBTC - Nova Scotia Junior Badminton Training Center
[**JBTC - Nova Scotia Junior Badminton Training Center**](https://www.nsjbtc.com/) 10,000 sqf of professional facilites dedicated to badminton training and competition, one of the top in Atlantic Canada. The address is 200 Bluewater Rd
Bedford NS, B4B1G9.

## Government

[**City Hall**](https://www.halifax.ca/) a home of government of Halifax, specific tourisms are allowed 

### Service Canada

[**Service Canada**](https://www.servicecanada.info/service-canada-halifax/) is an important place for international students and it is in 1800 Argyle St. If you have visa and tax requirements, you must remember this place.

[**Access Nova Scotia**](https://novascotia.ca/access-locations/) It is probably the only place for you to do the paper work for your motor vehicle. The walk in is now available, you don't have to book an appointment now.

[**Government House**](https://lt.gov.ns.ca/government-house) It is house for governor of Nova Scotia. It is a one of oldest existing government buildings in North America. The house has served for more than 200 years, it is a great Canadian national treasure. 

### City of Halifax 

[**City of Halifax**](https://www.halifax.ca/) If you want to pay for your property taxes or any building permits, you can go to City of Halifax. If you are going to get the street parking permits, you can go to City of Halifax. 

## Asia Supermarkets

[**LOONG 7 MART**](https://loong7mart-asian-grocery-store.business.site/?utm_source=gmb&utm_medium=referral) This is a supermarket that specializes in Chinese and Asian food, with a wide variety of Chinese snacks, such as instant noodles,
snail noodles, hot pot base. As well as some great tasting drinks, if you have the opportunity to come and see

[**Tai Shan mart**](https://goo.gl/maps/5urytQXNeuFR7Jaz7) it's a Chinese supermarket with a lot of Chinese snacks and daily necessities, I really like to buy hot pot base here.

### Union Foodmart

[**Union Foodmart**](https://www.google.ca/maps/place/Union+Foodmart+%E8%8F%AF%E8%81%AF/@44.6729347,-63.5851771,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a21940bd0ecfb:0x5bf091da6ba992c3!8m2!3d44.6729312!4d-63.5829834?hl=en&authuser=0) is located in Dartmouth. The largest Asia Supermarket that I have known. 

**[South Asian Mart](https://southasianmart.ca/)** offers a complete range of ethnic grocery from India, Pakistan, Bangladesh, Sri Lankan at very competitive prices.

[**Ca-Hoa Grocery**](https://goo.gl/maps/2PKXuKs4TddfP1uZA) is an old Asian shop. You can buy some really local Chinese source and cooking tool here.

[**OCO mart**](https://www.google.com/maps?q=oco+mart&um=1&ie=UTF-8&sa=X&ved=2ahUKEwjPvZ71n9X3AhWRlYkEHd-eAUEQ_AUoAXoECAIQAw) is a korean grocery store where they have variety of korean products.

[**M&Y Asian Grocery**](https://nicelocal.ca/halifax/shops/mywindsorasian_market/reviews/) is located at 6238 Quinpool Rd. It sells a wide range of ingredients, snacks, drinks, and sauce that are made in Asian countries.

[**Food & Nutrition**](https://foodnutrition.ca/contact) It not only focus on selling products, but also gives customers a new view on how to know their body well and how to choose and cook foods.

[**Shahi Groceries**](https://www.shahigrocery.com/index.php?route=common/home)  
A South Asian grocery store. You can get your choice of groceries and snacks from a selection of goods from your home country.

## Shopping Center

[**Halifax Shopping Centre**](https://halifaxshoppingcentre.com/) The biggest shopping center in Halifax. You can find all kinds of brands there and buy anything you want. Also, there is a Walmart which is opposite to it.

[**Mic Mac Mall**](https://micmacmall.com/) The biggest shopping center in Dartmouth, it offers you the opportunity to shop everything in Dartmouth without crossing the bridge.

[**IKEA Halifax**](https://www.ikea.com/ca/en/stores/halifax/?utm_source=google&utm_medium=organic&utm_campaign=map&utm_content=halifax) the biggest shopping center about furniture in Dartmouth(like shopping center but not exactly), if you want the newest furniture, then you can go there and find if some of them are suitable for you.

[**park lane mall**](http://shopparklane.ca/) a place to go shopping, watching movies, and browsing dollarama with your friends during the weekend

[**Scotia Square**](https://scotiasquare.com/) is a shopping centre near the Halifax Waterfront. It is probably best known for its large food court, featuring a variety of different restaurants. Connected to Scotia Square is a bus terminal for Halifax Transit, this makes Scotia Square a hub of sorts for those arriving in Halifax from areas like Dartmouth, giving greater attention to the shops and restaurants featured within. The close proximity to other areas such as the waterfront and Citadel Hill also make this a nice place to visit.

### Winners

[**Winners**](https://www.winners.ca/en) is a good place for shopping because everything is cheap with high quality. New cloth and designer fashions arrive several times a week.

### Park Lane Mall

[**Park Lane Mall**](http://shopparklane.ca/) is a shopping mall located on Spring Garden Road. With stores like Cleve's and Dollarama, and a Cineplex movie theatre, Park Lane mall is a great place to visit on your next Spring Garden shopping trip. 

### [Dartmouth Crossing](https://www.google.com/maps/place/Dartmouth+Crossing,+Dartmouth,+NS/@44.7013455,-63.576132,15z/data=!3m1!4b1!4m5!3m4!1s0x4b5a26bef921f91d:0xd31163dc6cc44ac8!8m2!3d44.7055743!4d-63.5637721)
Not really a shopping centre it is; however, I am sure you will want to visit at least one of the places here. There are lots of places to get new clothes for the Summer, as well as Costco and Ikea are there too! Plus if you get tired and hungry while you are shopping, there are great food places such as Montana’s or Boston Pizza as well. 




## Live Music
### Durty Nelly's
[**Durty Nelly's**](https://durtynellys.ca/) is a classic irish pub that has live music 7 nights a week! With a great selection of food & drinks it is a must-visit place in Halifax.

### The Old Triangle Irish Alehouse

[**The Old Triangle Irish Alehouse**](https://www.oldtriangle.com/welcome/) The Celtic Heart of the Maritime's! Experience the best of Irish cuisine and drink Halifax has to offer here on 5136 Prince St. Enjoy some of the greatest live music Nova Scotia artists have to offer!

### Split Crow Pub 
[**Split Crow Pub**](https://www.splitcrow.com) The Split Crow Pub is proud to be ‘Nova Scotia’s Original Tavern’ and continues to serve locals and travelers from around the world. A welcoming smile, generous mugs of grog, fantastic food and, of course, toe-tapping music, welcomes you at the Split Crow Pub. Great live music and $2.50 beers during power hour!

### Dalhousie Arts Centre
[**Dalhousie Arts Centre**](https://www.dal.ca/dept/arts-centre.html)
The Dalhousie Arts Centre hosts shows/concerts that are truly breath-taking. I’ve been lucky enough to go to a show by the legendary guitarist Tommy Emmanuel, and a show by the Symphony Nova Scotia playing pieces by my favourite classical composer, Maurice Ravel. They also do other forms of live media.

## Entertainment

![Captured Escape Rooms](images/escape_room.png)

<small>This image has been taken from https://capturedescaperooms.com/rooms/plunder, on May 8, 2022</small>

**[Captured Escape Rooms](https://capturedescaperooms.com/)** are designed for intense participation from the group trying to escape. It’s perfect for team building experience!

[**Red Bar**](http://www.redbar.ca/) is a karaoke located at 5680 Spring Garden Rd. You can not only show your beautiful voice there, but also enjoy some live music by various bands. 

[**Trapped Halifax**](https://trapped.com/locations/halifax.html) is a similar establishment located on Barington St, offering a variety of escape rooms for groups to attempt.

[**Propeller Arcade**](https://drinkpropeller.ca/pages/propeller-arcade) A retro arcade with many pinball machines, ski ball, and other arcade cabinets located in the lower level of Propeller Brewing Company's Gottingen Street taproom. The best pinball selection and my favourite arcade in Halifax. 

[**Scotiabank Theatre Halifax**](https://www.cineplex.com/Theatre/scotiabank-theatre-halifax) It is only theatre has IMAX in Halifax, which advanced in comfortable chair and good quaillty vision and sounds. 

[**Yuk Yuk's**](https://www.yukyuks.com/) is a local comedy club and a good place to go for a laugh!

### Nova Tactical

[**Nova Tactical**](https://www.novatactical.ca/) is a good place to experience the joy of shooting. The service includes archery ranges, airsoft ranges and gun ranges, you also can buy firearms and fishing tools.

### Halifax Ghost Walk

[**Halifax Ghost Walk**](https://www.novascotia.com/see-do/tours/halifax-ghost-walk/7926) Experience the haunted history of Halifax by attending the Halifax Ghost Walk tour located at The Old Town Clock on Brunswick Street. Learn about the thrilling events of Halifax's chilling run ins with pirates, ghost tales, and eerie happenings on this journey through the city!


### Get Air Trampoline Park

[**Get Air Trampoline Park**](https://getairsports.com/nova-scotia/) great place for kids and adults, cheap and very interesting. Great place to have activities indoors when the weather isn't sunny.




### Mersey Road Paintball

[**Mersey Road Paintball**](https://www.facebook.com/Mersey-Road-Paintball-301437796536617/) is a great oppourtunity to enjoy simulated combat for sport. The field offers walk-on options for new players where they can rent out gear and purchase paintballs on site. Mersey Road contains a mix of wooded areas and civilian style structures.

### The Deck Box

[**The Deck Box**](https://www.thedeckboxhalifax.com/) is a game store for trading card games, board games and video games, including but not limited to Magic the Gathering, Pokemon, Yu-Gi-oh, Dungeons and Dragons, etc. Deck Box also provides a place and space to play card games, as well as air conditioning, which is a good place for trading card game lovers to meet and communicate offline.

### Neptune Theatre

[**Neptune Theatre**](https://www.neptunetheatre.com) is a wonderful theatre located on Argyle Street. Neptune is a great place to watch live performances such as plays, musicals, and concerts.

### Scotiabank Centre

[**The Scotiabank Centre**](https://www.scotiabank-centre.com/) is the largest facility in Atlantic Canada. Home to the hockey team the Halifax Mooseheads and the Halifax Thunderbirds lacrosse team this multi-purpose arena hosts sports games, concerts, and has restaurants. 

### Bowlarama
[**Bowlarama**](https://southcentrebowlarama.ca/en) is a fun place to go to with family and friends. Contains an arcade, bowling alleys, a bar, and also food and drinks so you can easily make the visit a whole day event.

## Market

### Topfresh Market

[**Topfresh Market**](http://topfresh.ca/which) is at 1 Flamingo Dr. You can buy some unique Chinese ingredients, seasonings, and some kitchen utensils and tableware.
[**Google Map Link**](https://goo.gl/maps/inN2ic3BAez25DSCA)

[**Atlantic Superstore**](https://www.atlanticsuperstore.ca) is a Canadian supermarket chain. You can buy almost all kinds of daily necessities and fresh food.

## Retail Stores

[**The Black Market Boutique**](https://www.blackmarkethalifax.ca/) is an abundantly colorful store located on Grafton St. featuring crafts and goods from around the world.

[**The Loot**](https://www.instagram.com/theloothfx/?hl=en) is a curated thrift store selling vintage clothing, accessories, and art.

[**The Halifax Army Navy Store**](http://www.thehalifaxarmynavystore.net/), (also known as Ron's Army/Navy), is a family owned military surplus store located on Agricola St.

[**Strange Adventures: Comics & Curiosities**](http://www.strangeadventures.com/) Canada's oddest and award-winning comic book store, located in both downtown Halifax and downtown Dartmouth. Selling comics since 1992.

[**Pete's Frootique**](https://petes.ca/) is a grocery store with locations at Dresden Row in Halifax, as well as the Sunnyside Mall in Bedford. It has a wide selection of products that are hard to find elsewhere in other, larger grocery stores, including many international products from areas such as Great Britain. It also features a juice bar which serves many different smoothies and juices.

### Walmart 

[**Walmart**](https://www.walmart.ca/en) is one of the largest grocery stores in Halifax. You can find all kinds of groceries as well as electronics, home appliances, and stuff like curtains, cushions, and duvets. All at an affordable price. 

### The Monster Comic Lounge

[**Monster Comic Lounge**](https://www.facebook.com/MonsterComicLounge/) is a collectibles store located on Gottingen St. Here, you can purchase and exchange trading cards. Also, you can find a variety of other collectible items including figures, board games and comic books.

## Events

### Halifax Burger Bash

[**Halifax Burger Bash**](https://burgerbash.ca/) takes place every year from April 28 to May 7. During that time, Halifax restaurants will sell their creative burgers (more than 130, priced at $7 or more) and donate the proceeds from each burger to [**Feed Nova Scotia**](https://www.feednovascotia.ca/). If you're interested in burgers, don't miss it.

[**The Busker Festival**](https://www.buskers.ca) is an annual festival in Halifax showcasing the many diverse talents of busking performers from around the world. The festival ocurs in the summer and is a wonderful way to spend some time on the Halifax Waterfront. 

## Learning Opportunities
### Halifax Central Library Media Studios
[**The Halifax Central Library Media Studios**](https://www.halifaxpubliclibraries.ca/library-spaces/book-a-space/media-studio/) comes well-equipped with musical instruments, recording equipment, and top-quality software. You can even do video editing, since it has a green-screen in one of the studios. To top it off, you can rent out equipment, and learn various things through your library card which has access to LinkedIn Learning; and ALL of this is FREE!

### Farmers Markets

[**Bedford basin Farmers Market**](https://www.bedfordbasinmarket.com/) 
Greek owned farmers market, with a bistro, spice market, and imported goods from Europe (Mainly 
Greece and Germany) 

### Halifax Color Festival

[**Halifax Color Festival**](https://allevents.in/halifax/halifax-colour-festival-2022/200022309100406) is an annual event where people dance and play with colors. Food trucks are usually available if you want to grab something to eat. It is open to people from all ages. 


